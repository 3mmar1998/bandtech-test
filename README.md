## Prerequisites

- **[PHP >= 8.0.0]**
- **[Composer]**
- **[MySQL database]**

## Installation

##### 1-clone

```git clone https://gitlab.com/3mmar1998/bandtech-test.git```

##### 2-Navigate to the project directory

```cd your-project-directory```

##### 3-Install dependencies

```composer install```

##### 3-Generate an application key

```php artisan key:generate```

##### 4-generate storage link

```php artisan storage:link```


## Database Configuration

##### 1-Create a .env file in the project root directory.
##### 2-Copy the contents of .env.example to .env.
##### 3-Edit the .env file and update the following configuration:

```
DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=your_database_name
DB_USERNAME=your_database_user
DB_PASSWORD=your_database_password
```

## Database Migrations

##### Run the following command to migrate your database:

```php artisan migrate```


## Database seeding

##### Run the following command to seed your database with initial data:

```php artisan db:seed```


## Starting the Development Server

##### Run the following command to start the Laravel development server:

```php artisan serve```