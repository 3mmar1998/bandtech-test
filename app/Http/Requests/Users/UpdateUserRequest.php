<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $userId = $this->route()->id;

        return [
            'name' => ['required', 'string', 'max:255'],
            'user_name' => ['required', 'string', 'max:255', Rule::unique('users','user_name')->ignore($userId)],
            'email' => ['required', 'email', 'max:255', Rule::unique('users','email')->ignore($userId)],
            'avatar' => ['nullable', 'image', 'max:1024'],
            'type_id' => ['required', 'exists:user_types,id'],
            'password' => ['nullable', 'min:6', 'confirmed'],
        ];
    }
}
