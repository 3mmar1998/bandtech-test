<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Interfaces\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseController
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepositoryInterface)
    {
        $this->userRepository = $userRepositoryInterface;
    }

    public function register(RegisterRequest $request)
    {
        try {
            $user = $this->userRepository->createUser($request->validated());
            $token = $user->createToken('apiToken')->plainTextToken;
            return $this->sendSuccess('The registration has been done successfully', ['token' => $token, 'user' => new UserResource($user)]);
        } catch (\Throwable $e) {
            logger('Error while user registeration', [$e->getMessage()]);
            return $this->sendError('Something went error');
        }
    }

    public function login(LoginRequest $request)
    {
        $data = $request->validated();

        // matching the email/user name
        $user = User::where('email', $data['email'])->orWhere('user_name', $data['email'])->first();

        if (!$user || !Hash::check($data['password'], $user->password))
            return $this->sendError('The email or password is incorrect');

        if (!$user->is_active)
            return $this->sendError('The account requires activation');

        $token = $user->createToken('apiToken')->plainTextToken;
        return $this->sendSuccess('login has been done successfully', ['token' => $token, 'user' => new UserResource($user)]);
    }

    public function logout(Request $request)
    {
        $checkLogout = $request->user()->currentAccessToken()->delete();

        if ($checkLogout) {
            return $this->sendSuccess('logout has been done successfully');
        }

        return $this->sendError('Something went error');
    }
}
