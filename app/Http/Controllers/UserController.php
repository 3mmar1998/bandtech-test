<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\UpdateUserRequest;
use App\Http\Requests\Users\UserRequest;
use App\Http\Resources\UserResource;
use App\Interfaces\UserRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepositoryInterface)
    {
        $this->userRepository = $userRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = $this->userRepository->getAllUsers();
       
        return $this->sendResponse(UserResource::collection($users), 'Data retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserRequest $request)
    {
        try {
            $user = $this->userRepository->createUser($request->validated());
            return $this->sendSuccess('The user has been created successfully', new UserResource($user));
        } catch (\Throwable $e) {
            logger('Error while creating a user', [$e->getMessage()]);
            return $this->sendError('Something went error');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $user = $this->userRepository->getUserById($id);
            return $this->sendResponse(new UserResource($user), 'Data retrieved successfully');
        } catch (ModelNotFoundException $e) {
            return $this->sendError('NotFound', 'ERR404', null, 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, string $id)
    {
        try {
            $user = $this->userRepository->updateUser($id, $request->validated());
            return $this->sendSuccess('The user has been updated successfully', new UserResource($user));
        } catch (ModelNotFoundException $e) {
            return $this->sendError('NotFound', 'ERR404', null, 404);
        } catch (\Throwable $e) {
            logger('Error while updating a user', [$e->getMessage()]);
            return $this->sendError('Something went error');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $this->userRepository->deleteUser($id);
            return $this->sendSuccess('The user has been deleted successfully');
        } catch (ModelNotFoundException $e) {
            return $this->sendError('NotFound', 'ERR404', null, 404);
        } catch (\Throwable $e) {
            logger('Error while deleting a user', [$e->getMessage()]);
            return $this->sendError('Something went error');
        }
    }
}
