<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserTypeResource;
use App\Interfaces\UserTypeRepositoryInterface;
use Illuminate\Http\Request;

class UserTypeController extends BaseController
{
    private $userTypeRepository;

    public function __construct(UserTypeRepositoryInterface $userTypeRepositoryInterface)
    {
        $this->userTypeRepository = $userTypeRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $userTypes = $this->userTypeRepository->getAllUserTypes();
        
        return $this->sendResponse(UserTypeResource::collection($userTypes), 'Data retrieved successfully');
    }
}
