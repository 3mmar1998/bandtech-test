<?php

namespace App\Http\Controllers;

use App\Http\Requests\Products\ProductRequest;
use App\Http\Resources\ProductBasedOnTypeResource;
use App\Http\Resources\ProductResource;
use App\Interfaces\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ProductController extends BaseController
{
    private $ProductRepository;

    public function __construct(ProductRepositoryInterface $ProductRepositoryInterface)
    {
        $this->ProductRepository = $ProductRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = $this->ProductRepository->getAllProducts();

        return $this->sendResponse(ProductBasedOnTypeResource::collection($products), 'Data retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductRequest $request)
    {
        try {
            $product = $this->ProductRepository->createProduct($request->validated());
            return $this->sendSuccess('The product has been created successfully', new ProductResource($product));
        } catch (\Throwable $e) {
            logger('Error while creating a product', [$e->getMessage()]);
            return $this->sendError('Something went error');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $product = $this->ProductRepository->getProductById($id);
            return $this->sendResponse(new ProductBasedOnTypeResource($product), 'Data retrieved successfully');
        } catch (ModelNotFoundException $e) {
            return $this->sendError('NotFound', 'ERR404', null, 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductRequest $request, string $id)
    {
        try {
            $product = $this->ProductRepository->updateProduct($id, $request->validated());
            return $this->sendSuccess('The product has been updated successfully', new ProductResource($product));
        } catch (ModelNotFoundException $e) {
            return $this->sendError('NotFound', 'ERR404', null, 404);
        } catch (\Throwable $e) {
            logger('Error while updating a product', [$e->getMessage()]);
            return $this->sendError('Something went error');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $this->ProductRepository->deleteProduct($id);
            return $this->sendSuccess('The product has been deleted successfully');
        } catch (ModelNotFoundException $e) {
            return $this->sendError('NotFound', 'ERR404', null, 404);
        } catch (\Throwable $e) {
            logger('Error while deleting a product', [$e->getMessage()]);
            return $this->sendError('Something went error');
        }
    }
}
