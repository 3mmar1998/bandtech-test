<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * used for retrieving data (ex: queries data ...) 
     */
    public function sendResponse($result, $message = null)
    {
        $response = [
            'success' => true,
            'message' => $message,
            'data' => $result,
        ];

        return response()->json($response, 200);
    }

    /**
     * used when we have handling an error (ex: error while creating, updating ...)
     */
    public function sendError($message, $errorCode = 'ERR', $result = null, $httpCode = 200)
    {
        $response = [
            'success' => false,
            'message' => $message,
            'error_code' => $errorCode,
            'data' => $result,
        ];

        return response()->json($response, $httpCode);
    }

    /**
     * used when we have a success action (ex: create, update ...)
     */
    public function sendSuccess($message, $result = null)
    {
        $response = [
            'success' => true,
            'message' => $message,
            'data' => $result,
        ];

        return response()->json($response, 200);
    }
}
