<?php

namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface
{
    public function getAllUsers()
    {
        return User::with('type')->paginate(10);
    }

    public function getUserById(string $id)
    {
        return User::where('id', $id)->firstOrFail();
    }

    public function createUser(array $attributes)
    {
        $this->handlePasswordUpdate($attributes);
        $this->handleAvatarUpload($attributes);

        return User::create($attributes);
    }

    public function updateUser(string $id, array $attributes)
    {
        $user = $this->getUserById($id);

        $this->handlePasswordUpdate($attributes);
        $this->handleAvatarUpload($attributes);

        $user->update($attributes);

        return $user;
    }

    public function deleteUser(string $id)
    {
        $user = $this->getUserById($id);

        return $user->delete();
    }

    private function handlePasswordUpdate(array &$attributes)
    {
        if (isset($attributes['password'])) {
            $attributes['password'] = Hash::make($attributes['password']);
        } else {
            unset($attributes['password']); // Avoid unnecessary update
        }
    }

    private function handleAvatarUpload(array &$attributes)
    {
        if (isset($attributes['avatar'])) {
            $attributes['avatar'] = $attributes['avatar']->storePublicly('users', 'public');
        } else {
            unset($attributes['avatar']); // Avoid unnecessary update
        }
    }
}
