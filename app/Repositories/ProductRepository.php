<?php

namespace App\Repositories;

use App\Interfaces\ProductRepositoryInterface;
use App\Models\Product;
use App\Models\ProductPrice;
use Illuminate\Support\Facades\DB;

class ProductRepository implements ProductRepositoryInterface
{
    public function getAllProducts()
    {
        return Product::with('priceBasedOnUserType')->paginate(10);
    }

    public function getProductById(string $id)
    {
        return Product::where('id', $id)->firstOrFail();
    }

    public function createProduct(array $attributes)
    {
        DB::beginTransaction();

        try {
            $this->handleImageUpload($attributes);

            $product = Product::create($attributes);
            $this->createProudctPrices($product, $attributes['prices']);
            DB::commit();
            return $product;
        } catch (\Throwable $e) {
            DB::rollBack();
            logger('Error while creating a product', [$e->getMessage()]);
            throw $e;
        }
    }

    private function createProudctPrices($product, $attributes)
    {
        foreach ($attributes as $price) {
            $product->prices()->attach($price['type_id'], ['price' => $price['value']]);
        }
    }

    public function updateProduct(string $id, array $attributes)
    {
        DB::beginTransaction();

        try {
            $this->handleImageUpload($attributes);

            $product = $this->getProductById($id);
            $product->update($attributes);
            $this->updateProductPrices($product, $attributes['prices']);
            DB::commit();
            return $product;
        } catch (\Throwable $e) {
            DB::rollBack();
            logger('Error while updating a product', [$e->getMessage()]);
            throw $e;
        }
    }

    private function updateProductPrices($product, $attributes)
    {
        foreach ($attributes as $price) {
            ProductPrice::where(['product_id' => $product->id, 'type_id' => $price['type_id']])->update(['price' => $price['value']]);
        }
    }

    public function deleteProduct(string $id)
    {
        $product = $this->getProductById($id);

        return $product->delete();
    }

    private function handleImageUpload(array &$attributes)
    {
        if (isset($attributes['image'])) {
            $attributes['image'] = $attributes['image']->storePublicly('users', 'public');
        } else {
            unset($attributes['image']); // Avoid unnecessary update
        }
    }
}
