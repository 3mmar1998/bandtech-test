<?php

namespace App\Interfaces;

interface UserRepositoryInterface
{
    public function getAllUsers();
   
    public function getUserById(string $id);
   
    public function createUser(array $attributes);
   
    public function updateUser(string $id, array $attributes);
   
    public function deleteUser(string $id);
}
