<?php

namespace App\Interfaces;

interface ProductRepositoryInterface
{
    public function getAllProducts();
   
    public function getProductById(string $id);
   
    public function createProduct(array $attributes);
   
    public function updateProduct(string $id, array $attributes);
   
    public function deleteProduct(string $id);
}
