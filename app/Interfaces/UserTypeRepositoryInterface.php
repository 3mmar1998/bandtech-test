<?php

namespace App\Interfaces;

interface UserTypeRepositoryInterface
{
    public function getAllUserTypes();
}
