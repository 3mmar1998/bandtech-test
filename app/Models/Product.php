<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use HasFactory, Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'image',
        'is_active',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function prices()
    {
        return $this->belongsToMany(UserType::class, 'product_prices', 'product_id', 'type_id')->withPivot(['price']);
    }

    public function priceBasedOnUserType()
    {
        $query = $this->belongsToMany(UserType::class, 'product_prices', 'product_id', 'type_id')->withPivot(['price']);

        if (auth('sanctum')->check()) {
            return $query->wherePivot('type_id', auth('sanctum')->user()->type_id);
        }

        return $query;
    }
}
